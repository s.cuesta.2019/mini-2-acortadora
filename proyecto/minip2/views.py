from django.shortcuts import redirect, render
from django.http import HttpResponse, HttpResponse, Http404
from django.urls import NoReverseMatch
from django.views.decorators.csrf import csrf_exempt
from .models import Contenido
import os

URL_PREFIXES = ("http://", "https://", "www.")
ERROR_URL_PREFIX = "La URL debe comenzar por 'http://', 'https://' o 'www.', por favor intentelo de nuevo"
ERROR_CONTENT_NOT_FOUND = "El recurso {} no existe"
ERROR_INVALID_URL = "La URL {} no es válida"

def get_counter():
    last_short = Contenido.objects.order_by('-short').first()
    return int(last_short.short) + 1 if last_short else 1

def get_main(request):
    return render(request, 'minip2/main.html', {'url_acortar': "acortar/"})

@csrf_exempt
def index(request):
    if request.method == "POST":
        url = request.POST.get("url", "").strip()
        if url:
            if not url.startswith(URL_PREFIXES):
                return HttpResponse(ERROR_URL_PREFIX)
            if not Contenido.objects.filter(url__iexact=url).exists():
                short = request.POST.get("short") or str(get_counter())
                Contenido.objects.create(url=url, short=short)
    content_list = Contenido.objects.all()
    return render(request, 'minip2/pag_inicio.html', {'content_list': content_list})

@csrf_exempt
def get_content(request, llave):
    try:
        contenido = Contenido.objects.get(short=llave)
        return redirect(contenido.url)
    except Contenido.DoesNotExist:
        raise Http404(ERROR_CONTENT_NOT_FOUND.format(llave))
    except NoReverseMatch:
        raise Http404(ERROR_INVALID_URL.format(contenido.url))

def get_favicon(request):
    favicon_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static', 'favicon.ico')
    try:
        with open(favicon_path, "rb") as f:
            favicon = f.read()
        return HttpResponse(favicon, content_type="image/x-icon")
    except FileNotFoundError:
        return HttpResponse(status=404)
